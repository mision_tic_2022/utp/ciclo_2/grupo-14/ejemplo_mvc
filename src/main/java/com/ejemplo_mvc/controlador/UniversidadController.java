package com.ejemplo_mvc.controlador;

import java.util.ArrayList;

import com.ejemplo_mvc.modelo.Universidad;

public class UniversidadController {
    // ATRIBUTOS
    private ArrayList<Universidad> universidades;

    public UniversidadController() {
        this.universidades = new ArrayList<Universidad>();
    }

    public int cantUniversidades() {
        return universidades.size();
    }

    public void crearUniversidad(String nit, String nombre, String direccion, String email) {
        // Crear universidad
        Universidad objUni = new Universidad(nit, nombre, direccion, email);
        // Agregar el objeto al arrayList
        universidades.add(objUni);
    }

    public String consultarUniversidad(String nit) {
        String info = "";
        for (Universidad universidad : universidades) {
            if (universidad.getNit().equalsIgnoreCase(nit)) {
                info = universidad.toString();
                break;
            }
        }
        return info;
    }

    public String consultarXindex(int index) {
        String info = "";
        if (index >= 0 && index < universidades.size()) {
            info = universidades.get(index).toString();
        }
        return info;
    }

    public void actualizarUniversidad(String nit, String nombre, String direccion, String email) {
        for (int i = 0; i < universidades.size(); i++) {
            if (universidades.get(i).getNit().equalsIgnoreCase(nit)) {
                // Actualizar información
                universidades.get(i).setNombre(nombre);
                universidades.get(i).setDireccion(direccion);
                universidades.get(i).setEmail(email);
                break;
            }
        }
    }

    // ELIMINAR UNIVERSIDAD POR NIT
    public boolean eliminarUniversidad(String nit) {
        boolean delete = false;
        int index = -1;
        // Buscar la universidad por nit
        for (int i = 0; i < universidades.size(); i++) {
            if (universidades.get(i).getNit().equalsIgnoreCase(nit)) {
                index = i;
                break;
            }
        }
        // Eliminar universidad
        if (index >= 0) {
            universidades.remove(index);
            delete = true;
        }
        return delete;
    }

}
