package com.ejemplo_mvc.vista;

import java.util.Scanner;

import com.ejemplo_mvc.controlador.UniversidadController;

public class UniversidadVista {
    // ATRIBUTOS
    private UniversidadController uController;

    // CONSTRUCTOR
    public UniversidadVista() {
        uController = new UniversidadController();
    }

    public void crearUniversidad(Scanner sc) {
        System.out.println("\n---------------CREAR UNIVERSIDAD--------------\n");
        System.out.println("Ingrese la siguiente informacion: ");
        // ------------Solicitar datos al usuario------------
        // Solicitar nit
        System.out.print("Nit: ");
        String nit = sc.next();
        sc.nextLine();
        // Solicitar nombre
        System.out.print("Nombre: ");
        String nombre = sc.next();
        sc.nextLine();
        // Solicitar dirección
        System.out.print("Direccion: ");
        String direccion = sc.next();
        sc.nextLine();
        // Solicitar email
        System.out.print("Email: ");
        String email = sc.next();
        sc.nextLine();
        // CREAR UNIVERSIDAD
        uController.crearUniversidad(nit, nombre, direccion, email);
        System.out.println("\n\nUNIVERSIDAD CREADA CON EXITO 💪 👊\n\n");
    }

    public void mostrarUniversidades() {
        String universidades = "\n\n----------------UNIVERSIDADES REGISTRADAS--------------\n";
        for (int i = 0; i < uController.cantUniversidades(); i++) {
            universidades += uController.consultarXindex(i);
        }
        System.out.println(universidades);
    }

    public void mostrarUniversidadXnit(Scanner sc) {
        System.out.println("\n\n------------CONSULTAR UNIVERSIDAD POR NIT----------------\n");
        System.out.print("Nit: ");
        String nit = sc.next();
        sc.nextLine();
        String info = uController.consultarUniversidad(nit);
        System.out.println(info);
    }

    public void actualizarUniversidad(Scanner sc) {
        // Encabezado
        System.out.println("\n\n--------------ACTUALIZAR UNIVERISDAD---------\n");
        System.out.println("Por favor ingrese la siguiente información: ");
        // Solicitar nit
        System.out.print("Nit: ");
        String nit = sc.next();
        sc.nextLine();
        // Solicitar nombre
        System.out.print("Nombre: ");
        String nombre = sc.next();
        sc.nextLine();
        // Solicitar dirección
        System.out.print("Direccion: ");
        String direccion = sc.next();
        sc.nextLine();
        // Solicitar email
        System.out.print("Email: ");
        String email = sc.next();
        sc.nextLine();
        // Actualizar universidad
        uController.actualizarUniversidad(nit, nombre, direccion, email);
        System.out.println("\nUniversidad actualizada con exito");
    }

    public void eliminarUniversidad(Scanner sc) {
        // ENCABEZADO
        System.out.println("\n\n----------------ELIMINAR UNIVERSIDAD---------------\n");
        System.out.println("Por favor ingrese la siguiente informacion: ");
        // Solicitar nit
        System.out.print("Nit: ");
        String nit = sc.next();
        sc.nextLine();
        // Eliminar universidad
        if (uController.eliminarUniversidad(nit)) {
            System.out.println("\n\nUniversidad eliminada con exito\n\n");
        } else {
            System.out.println("\n\nNo existe una universidad en la BD con el nit ingresado\n\n");
        }
    }

    public void menu() {
        String mensaje = "----------------UNIVERSIDADES------------\n";
        mensaje += "1) Crear universidad\n";
        mensaje += "2) Consultar todas las universidades\n";
        mensaje += "3) Consultar universidad por nit\n";
        mensaje += "4) Actualizar universidad por nit\n";
        mensaje += "5) Eliminar universidad\n";
        mensaje += "-1) Salir\n";
        mensaje += ">>> ";
        int opc = 0;
        Scanner sc = new Scanner(System.in);
        while (opc != -1) {
            System.out.print(mensaje);
            opc = sc.nextInt();
            // Evaluar la opción ingresada
            switch (opc) {
                case 1:
                    crearUniversidad(sc);
                    break;
                case 2:
                    mostrarUniversidades();
                    break;
                case 3:
                    mostrarUniversidadXnit(sc);
                    break;
                case 4:
                    actualizarUniversidad(sc);
                    break;
                case 5:
                    eliminarUniversidad(sc);
                    break;
                default:
                    break;
            }
        }
    }

}
