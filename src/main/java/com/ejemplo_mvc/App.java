package com.ejemplo_mvc;

import com.ejemplo_mvc.vista.UniversidadVista;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        UniversidadVista universidadUI = new UniversidadVista();
        universidadUI.menu();
    }
}
